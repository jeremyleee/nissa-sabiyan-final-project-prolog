:- [service].

:- dynamic user_symptom/1.

analyze:-
    repeat,
    nl,
    write('------- Nissa Sabyan Clinic -------'), nl,nl,
    write("1. Diseases analysis "), nl,
    write("2. Check possible diseases by symptoms"), nl,
    write("3. Hospitals Menu"), nl,
    write("0. Quit"), nl, nl,
    write("How can we help you? (end with .): "),
    read(X),
    ( X = 0 -> !, fail ; true ),
    process_input(X),
    fail.

process_input(X):-
     ( X =:= 1 ->
     diseases_analysis_menu
    ;
    ( X =:= 2 ->
        by_symptoms_menu
    ;
        hospital_services_menu
        )
    ).

diseases_analysis_menu:-
    repeat,
    nl,
    write('------- Diseases Analysis -------'), nl,nl,
    write("1. Get all diseases"),
    nl,
    write("2. Analyze disease"),
    nl,
    write("0. Back"),
    nl,
    nl,
    write("Please choose service? (end with .): "),
    read(Service),
    ( Service = 0 -> !, fail ; true ),
    process_diseases_analysis_services(Service),
    fail.
   

process_diseases_analysis_services(Service):-
     ( Service =:= 1 ->
     get_all_diseases
    ;
    ( Service =:= 2 ->
     analyze_diseases
    ;
       analyze
        )
    ).

 analyze_diseases:-
    nl,
    write("Please input disease name: "),
    read(Disease),
    atom_concat(Disease, ' symptoms:', X), 
    write(X), nl,
    get_symptoms(Disease), nl, nl,
    atom_concat(Disease, ' treatments:', Z),
    write(Z), nl,
    get_treatments(Disease).

by_symptoms_menu:- 
    repeat,
    nl,
    write('------- Possible Diseases by Symptoms -------'), nl,nl,
    write("1. Get all symptoms"),
    nl,
    write("2. Check possible diseases"),
    nl,
    write("0. Back"),
    nl,
    nl,
    write("Please choose service? (end with .): "),
    read(Service),
    ( Service = 0 -> !, fail ; true ),
    process_by_symptoms_services(Service),
    fail.     


process_by_symptoms_services(Service):-
     ( Service =:= 1 ->
     get_all_symptoms
    ;
    ( Service =:= 2 ->
     check_possible_diseases_by_symptoms
    ;
       analyze
        )
    ).

check_possible_diseases_by_symptoms:-
    write("Input cont. to check possible diseases with given symptoms."),nl,
    repeat,
    write("Please input symptoms (e.g chills,headache,skin rash): "),
    read(Symptom),
    ( Symptom = cont ->
        get_possible_diseases_by_symptoms, retractall(user_symptom(_)), !
    ;
       assert(user_symptom(Symptom))
    ),
    fail.

get_possible_diseases_by_symptoms:-
    findall(X, user_symptom(X), Symptoms),
    nl,
    get_possible_diseases(Symptoms, L),
    nl,
    write('Possible diseases with given symptoms: '), nl,
    print_elements(L),nl,nl,
    write("Possible treatments for diseases mentioned: "), nl,nl,
    get_possible_treatments(L).

hospital_services_menu:-
    repeat,
    nl,
    write('------- Hospitals Menu -------'), nl,nl,
    write("1. Get all hospitals"),
    nl,
    write("2. Get all region covered"),
    nl,
    write("3. Get hospitals in certain region"),
    nl,
    write("0. Back"),
    nl,
    nl,
    write("Please choose service? (end with .): "),
    read(Service),
    ( Service = 0 -> !, fail ; true ),
    process_hospital_services(Service),
    fail.

process_hospital_services(Service):-
     ( Service =:= 1 ->
     get_all_hospitals
    ;
    ( Service =:= 2 ->
     get_all_region
    ;
       ( Service =:= 3 ->
            get_hospitals_by_region_sservice
       )
        )
    ).

get_hospitals_by_region_sservice:-
    nl,
    write("Please input region name: "),
    read(Region),
    get_hospitals_by_region(Region).