# Disease Analyzer Service
Disease Analyzer using SWI-Prolog created by Nissa Sabyan Team.

Nissa Sabyan team member:
1. Jeremy 1706039742
2. Jose Devian 1706039603
3. Naufaldi Athallah Rifqi 1706023782

## References
### Diseases symptoms database reference
[https://www.kaggle.com/itachi9604/disease-symptom-description-dataset?select=dataset.csv](https://www.kaggle.com/itachi9604/disease-symptom-description-dataset?select=dataset.csv)

### Diseases treatments database reference
[https://www.medicinenet.com/diseases_and_conditions/article.htm](https://www.medicinenet.com/diseases_and_conditions/article.htm)

### Hospitals database reference
[https://data.jakarta.go.id/dataset/rumahsakitdijakarta/resource/5d550b55-52c0-4d71-aa12-c4d319a593e8](https://data.jakarta.go.id/dataset/rumahsakitdijakarta/resource/5d550b55-52c0-4d71-aa12-c4d319a593e8)