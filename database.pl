% Gerd symptoms
symptom('gerd', 'stomach pain').
symptom('gerd', 'acidity').
symptom('gerd', 'vomiting').
symptom('gerd', 'cough').
symptom('gerd', 'chest pain').
symptom('gerd', 'ulcers on tongue').

% Diabetes symptoms
symptom('diabetes', 'fatigue').
symptom('diabetes', 'weight loss').
symptom('diabetes', 'restlessness').
symptom('diabetes', 'lethargy').
symptom('diabetes', 'irregular sugar level').
symptom('diabetes', 'obesity').
symptom('diabetes', 'excessive hunger').
symptom('diabetes', 'increased appetite').
symptom('diabetes', 'polyuria').

% Hypertension symptoms
symptom('hypertension', 'headache').
symptom('hypertension', 'chest pain').
symptom('hypertension', 'dizziness').
symptom('hypertension', 'loss of balance').
symptom('hypertension', 'lack of concentration').

% Migraine symptoms
symptom('migraine', 'acidity').
symptom('migraine', 'indigestion').
symptom('migraine', 'blurred and distorted vision').
symptom('migraine', 'excessive hunger').
symptom('migraine', 'headache').
symptom('migraine', 'stiff neck').
symptom('migraine', 'depression').
symptom('migraine', 'irritability').
symptom('migraine', 'visual_disturbances').

% Malaria symptoms
symptom('malaria', 'chills').
symptom('malaria', 'vomiting').
symptom('malaria', 'high fever').
symptom('malaria', 'sweating').
symptom('malaria', 'nausea').
symptom('malaria', 'muscle pain').
symptom('malaria', 'diarrhoea').

% Dengue symptoms
symptom('dengue', 'chills').
symptom('dengue', 'skin rash').
symptom('dengue', 'joint pain').
symptom('dengue', 'vomiting').
symptom('dengue', 'high fever').
symptom('dengue', 'headache').
symptom('dengue', 'loss of appetite').
symptom('dengue', 'pain behind the eyes').

% Hepatitis A symptoms 
symptom('hepatitis a', 'joint pain').
symptom('hepatitis a', 'vomiting').
symptom('hepatitis a', 'yellowish skin').
symptom('hepatitis a', 'dark urine').
symptom('hepatitis a', 'nausea').
symptom('hepatitis a', 'loss of appetite').
symptom('hepatitis a', 'abdominal pain').
symptom('hepatitis a', 'diarrhoea').
symptom('hepatitis a', 'mild fever').

% Hepatitis B symptoms 
symptom('hepatitis b', 'lethargy').
symptom('hepatitis b', 'fatigue').
symptom('hepatitis b', 'yellowish skin').
symptom('hepatitis b', 'itching').
symptom('hepatitis b', 'malaise').
symptom('hepatitis b', 'loss of appetite').
symptom('hepatitis b', 'abdominal_pain').
symptom('hepatitis b', 'dark urine').
symptom('hepatitis b', 'yellowing of eyes').
symptom('hepatitis b', 'yellow urine').

% Hepatitis c symptoms 
symptom('hepatitis c', 'nausea').
symptom('hepatitis c', 'fatigue').
symptom('hepatitis c', 'loss of appetite').
symptom('hepatitis c', 'family history').
symptom('hepatitis c', 'yellowing of eyes').

% Tuberculosis symptoms 
symptom('tuberculosis', 'chills').
symptom('tuberculosis', 'fatigue').
symptom('tuberculosis', 'vomiting').
symptom('tuberculosis', 'weight loss').
symptom('tuberculosis', 'cough').
symptom('tuberculosis', 'high fever').
symptom('tuberculosis', 'breathlessness').
symptom('tuberculosis', 'sweating').
symptom('tuberculosis', 'loss of appetite').
symptom('tuberculosis', 'mild fever').

% Cold symptoms
symptom('cold', 'continuous sneezing').
symptom('cold', 'chills').
symptom('cold', 'high fever').
symptom('cold', 'fatigue').
symptom('cold', 'headache').
symptom('cold', 'swelled lymph nodes').
symptom('cold', 'malaise').
symptom('cold', 'phlegm').
symptom('cold', 'throat irritation').

% Pneumonia symptoms
symptom('pneumonia', 'breathlessness').
symptom('pneumonia', 'chills').
symptom('pneumonia', 'high fever').
symptom('pneumonia', 'fatigue').
symptom('pneumonia', 'sweating').
symptom('pneumonia', 'malaise').
symptom('pneumonia', 'phlegm').
symptom('pneumonia', 'chest pain').
symptom('pneumonia', 'fast heart rate').

% Heart attack symptoms
symptom('heart attack', 'vomiting').
symptom('heart attack', 'chest pain').
symptom('heart attack', 'sweating').

% Acne symptoms
symptom('acne', 'skin rash').
symptom('acne', 'pus filled pimples').
symptom('acne', 'blackheads').
symptom('acne', 'scurring').

% AIDS symptoms
symptom('aids', 'muscle wasting').
symptom('aids', 'patches in throat').
symptom('aids', 'high fever').
symptom('aids', 'extra marital contacts').

% Allergy symptoms
symptom('allergy', 'continuous sneezing').
symptom('allergy', 'shivering').
symptom('allergy', 'chills').
symptom('allergy', 'watering from eyes').

% Bronchial Asthma
symptom('bronchial asthma', 'fatigue').
symptom('bronchial asthma', 'cough').
symptom('bronchial asthma', 'high fever').
symptom('bronchial asthma', 'breathlessness').
symptom('bronchial asthma', 'family histopry').
symptom('bronchial asthma', 'mucoid sputum').

% Chicken Pox
symptom('chicken pox', 'itching').
symptom('chicken pox', 'skin rash').
symptom('chicken pox', 'fatigue').
symptom('chicken pox', 'lethargy').
symptom('chicken pox', 'high fever').
symptom('chicken pox', 'loss of appetite').
symptom('chicken pox', 'mild fever').
symptom('chicken pox', 'swelled lymph nodes').

% COVID-19
symptom('covid-19', 'fever').
symptom('covid-19', 'dry cough').
symptom('covid-19', 'shortness of breath').
symptom('covid-19', 'sore throat').
symptom('covid-19', 'loss of taste').
symptom('covid-19', 'loss of smell').
symptom('covid-19', 'rash').
symptom('covid-19', 'chest pain').
symptom('covid-19', 'chest pressure').
symptom('covid-19', 'loss of speech').
symptom('covid-19', 'loss of movement').
symptom('covid-19', 'tiredness').
symptom('covid-19', 'body ache').
symptom('covid-19', 'diarrhoea').
symptom('covid-19', 'headache').

% Cholera
symptom('cholera', 'diarrhoea').
symptom('cholera', 'vomiting').
symptom('cholera', 'muscle cramps').

% Ebola
symptom('ebola', 'fever').
symptom('ebola', 'malaise').
symptom('ebola', 'headache').
symptom('ebola', 'chills').
symptom('ebola', 'muscle pain').

% Severe Acute Respiratory Syndrome (SARS)
symptom('sars', 'fever').
symptom('sars', 'myalgia').
symptom('sars', 'lethargy').
symptom('sars', 'cough').
symptom('sars', 'sore throat').
symptom('sars', 'non-specific').

% Middle East Respiratory Syndrome (MERS)
symptom('mers', 'fever').
symptom('mers', 'cough').
symptom('mers', 'shortness of breath').
symptom('mers', 'nausea').
symptom('mers', 'vomiting').
symptom('mers', 'diarrhoea').
symptom('mers', 'chest pain').
symptom('mers', 'fatigue').
symptom('mers', 'chills').
symptom('mers', 'body ache').
symptom('mers', 'sneezing').

% GERD treatments
treatment('gerd', 'antacids').
treatment('gerd', 'proton pump inhibitor').
treatment('gerd', 'H2 receptor blockers').

% Diabetes treatments
treatment('diabetes', 'insulin').
treatment('diabetes', 'healthy eating').
treatment('diabetes', 'physical activity').
treatment('diabetes', 'metformin').

% Hypertension treatments
treatment('hypertension', 'physical activity').
treatment('hypertension', 'low sodium and low fat diet').
treatment('hypertension', 'diuretics (water pills)').
treatment('hypertension', 'angiotensin converting enzyme inhibitors').
treatment('hypertension', 'angiotensin II receptor blockers').

% Migraine treatments
treatment('migraine', 'aspirin').
treatment('migraine', 'ibuprofen').
treatment('migraine', 'triptans').
treatment('migraine', 'lasmiditan').
treatment('migraine', 'ubrogepant (ubrelvy)').
treatment('migraine', 'dihydroergotamines ').

% Acne treatments
treatment('acne', 'skin care').
treatment('acne', 'face wash').
treatment('acne', 'soap containing salicylic acid').

% Heart Attack treatments
treatment('heart attack', 'prescribed medication').
treatment('heart attack', 'pain reliever').
treatment('heart attack', 'ace inhibitors').

% Pneumonia treatments
treatment('pneumonia', 'prescribed antibiotics').
treatment('pneumonia', 'rest').
treatment('pneumonia', 'rehydration').

% Cold treatments
treatment('cold', 'rest').
treatment('cold', 'rehydration').
treatment('cold', 'eat warm fluids').

% Tuberculosis treatments
treatment('tuberculosis', 'prescribed antibiotics').
treatment('tuberculosis', 'prescribed medications').

% Hepatitis C treatments
treatment('hepatitis c', 'anitiviral drug').
treatment('hepatitis c', 'liver transplant').

% Hepatitis A treatments
treatment('hepatitis a', 'rest').
treatment('hepatitis a', 'rehydration').

% Dengue treatments
treatment('dengue', 'oral rehydration').
treatment('dengue', 'prescribed painkillers').
treatment('dengue', 'prescribed medications').

% Malaria treatments
treatment('malaria', 'chloroquine (prescribed)').
treatment('malaria', 'doxycycline (prescribed)').
treatment('malaria', 'quinine (prescribed)').
treatment('malaria', 'mefloquine (prescribed)').
treatment('malaria', 'primaquine phosphate').

% Hepatitis B treatments
treatment('hepatitis b', 'antiviral drug').
treatment('hepatitis b', 'avoid alcohol (lifestyle)').

% AIDS treatments
treatment('aids', 'antiretroviral regimens').

% Chicken Pox treatments
treatment('chicken pox', 'antiviral drug').
treatment('chicken pox', 'analgesic').
treatment('chicken pox', 'antihistamin').
treatment('chicken pox', 'topical ointments').

% Bronchial Asthma treatments
treatment('bronchial asthma', 'bronchodilator').
treatment('bronchial asthma', 'steroid').
treatment('bronchial asthma', 'anti-inflammatory').

% Allergy treatments
treatment('allergy', 'antihistamins').
treatment('allergy', 'avoid allergens').

% COVID-19 treatments
treatment('covid-19', 'hospitalization').
treatment('covid-19', 'dexamethasone (prescribed)').

% Cholera treatments
treatment('cholera', 'oral rehydration therapy').
treatment('cholera', 'zinc supplementation').
treatment('cholera', 'antibiotics').
treatment('cholera', 'intravenous fluids').

% Ebola treatments
treatment('ebola', 'blood transfusion').
treatment('ebola', 'iv fluids').
treatment('ebola', 'oxygen therapy').

% SARS treatments
treatment('sars', 'antipyretics').
treatment('sars', 'mechanical ventilation').

% MERS treatments
treatment('mers', 'rest').
treatment('mers', 'oxygen therapy').
treatment('mers', 'hospitalization').

% Hospitals

% Hospitals in Central Jakarta
hospital('jakarta pusat', 'RS Islam Jakarta, Jl. Cempaka Putih Tengah I / 1, Cempaka Putih').
hospital('jakarta pusat', 'RS Pertamina Jaya, Jl. Achmad Yani No. 2, By Pass, Cempaka Putih').
hospital('jakarta pusat', 'RSUD Tarakan, Jl. Kyai Caringin No. 7, Gambir').
hospital('jakarta pusat', 'RS Mitra Kemayoran, Jl. Landas Pacu Timur, Kemayoran').
hospital('jakarta pusat', 'RS PGI Cikini, Jl. Raden Saleh No. 40, Menteng').
hospital('jakarta pusat', 'RS Abdi Waluyo, Jl. HOS Cokroaminoto No. 31 - 33, Menteng').
hospital('jakarta pusat', 'RSUPN Dr. Cipto Mangunkusumo, Jl. Diponegoro No. 71, Senen').
hospital('jakarta pusat', 'RSPAD Gatot Subroto, Jl. Dr. Abdul Rachman Saleh 24, Senen').
hospital('jakarta pusat', 'RSIA Evasari, Jl. Rawamangun No. 47, Cempaka Putih').
hospital('jakarta pusat', 'RS Khusus THT Profesor Nizar, Jl. Kesehatan No. 9, Gambir').
hospital('jakarta pusat', 'RS Khusus Gangguan Jiwa Dharma Sakti, Jl. Kaji No. 40, Gambir').
hospital('jakarta pusat', 'RS Khusus Mata Jakarta Eye Center, Jl. Teuku Cik Ditiro No. 46, Menteng').
hospital('jakarta pusat', 'RS Khusus Gigi FKG Universitas Indonesia, Jl. Salemba Raya, Senen').
hospital('jakarta pusat', 'RS Khusus Bedah, Salemba Satu, Jl. Salemba I  No. 13, Senen').

% Hospitals in Northern Jakarta
hospital('jakarta utara', 'RS Islam Jakarta Sukapura, Jl. Tipar Cakung No. 5, Cilincing').
hospital('jakarta utara', 'RS Gading Pluit, Jl. Boulevard Timur Raya RT. 006 / 02, Kelapa Gading').
hospital('jakarta utara', 'RS Mitra Keluarga Kelapa Gading, Jl. Bukit Gading Raya Kav. II, Kelapa Gading').
hospital('jakarta utara', 'RSUD Koja, Jl. Deli No. 4, Koja').
hospital('jakarta utara', 'RS Royal Progress, Jl. Danau Sunter Utara Raya No. 1, Tanjung Priok').
hospital('jakarta utara', 'RS Penyakit Infeksi Prof. Sulianti Saroso, Jl. Baru Sunter Permai Raya, Tanjung Priok').
hospital('jakarta utara', 'RS Khusus Paru Firdaus, Jl. Siak J-5 No. 14, Cilincing').
hospital('jakarta utara', 'RSIA Family, Pluit Mas I Blok A No. 2A - 5A, Penjaringan').
hospital('jakarta utara', 'RSIA Hermina Podomoro, Jl. Danau Agung 2 Blok E 3 No. 28-30, Tanjung Priok').

% Hospitals in Western Jakarta
hospital('jakarta barat', 'RSUD Cengkareng, Jl. Kamal Raya, Bumi Cengkareng Indah, Cengkareng').
hospital('jakarta barat', 'RS Sumber Waras, Jl. Kyai Tapa No. 1, Grogol Petamburan').
hospital('jakarta barat', 'RS Hermina Daan Mogot, Jl. Kintamani Raya No. 2, Kawasan Daan Mogot Baru, Kalideres').
hospital('jakarta barat', 'RS Siloam Hospitals Kebon Jeruk, Jl. Raya Pejuangan Kav. 8, Kebun Jeruk').
hospital('jakarta barat', 'RS Khusus Bedah Cinta Kasih Tzu Chi, Jl. Raya Kamal Outer Ring Road, Cengkareng').
hospital('jakarta barat', 'RS Khusus Jiwa Dr. Soeharto Heerdjan, Jl. Prof. Dr. Latumeten No. 1, Grogol Petamburan').
hospital('jakarta barat', 'RSIA Bina Sehat Mandiri, Jl. Duri Raya No. 22, Kedoya').
hospital('jakarta barat', 'RS Khusus Kanker Dharmais, Jl. Letjen S. Parman Kav. 84-86, Palmerah').
hospital('jakarta barat', 'RS Khusus Jantung dan Pembuluh Darah Harapan Kita, Jl. LetJen S. Parman Kav. 87, Palmerah').
hospital('jakarta barat', 'RS Khusus Gigi & Mulut FKG Universitas Trisakti, Jl. Kyai Tapa No. 1, Grogol Petamburan').
hospital('jakarta barat', 'RS Khusus Bersalin Anggrek Mas, Jl. Anggrek No. 2 B, Kebun Jeruk').

% Hospitals in Southern Jakarta
hospital('jakarta selatan', 'RS BAN Jakarta Selatan, Jl. Pesanggrahan No. 1, Bintaro').
hospital('jakarta selatan', 'RSUP Fatmawati, Jl. RS. Fatmawati, Cilandak').
hospital('jakarta selatan', 'RS Mayapada, Jl. Lebak Bulus 1, Cilandak').
hospital('jakarta selatan', 'RS Pondok Indah Jakarta Selatan, Jl. Metro Duta Kav. UE,  Pondok Indah, Kebayoran Lama').
hospital('jakarta selatan', 'RS Bhayangkara Selapa Polri, Jl. Ciputat Raya No. 40, Kebayoran Lama').
hospital('jakarta selatan', 'RS Jakarta Medical Center, Jl. Warung Buncit Raya No. 15, Pancoran').
hospital('jakarta selatan', 'RS Rumkital Marinir Cilandak, Jl. Raya Cilandak KKO No 3, Pasar Minggu').
hospital('jakarta selatan', 'RS Khusus Jiwa Dharmawangsa, Jl. Dharmawangsa Raya No. 13  Blok P II, Kebayoran Baru').
hospital('jakarta selatan', 'Rumah Sakit Khusus THT dan Bedah Yurino Ciranjang, Jl. Ciranjang  II No. 20-22, Kebayoran Baru').
hospital('jakarta selatan', 'RS Yadika, Jl. Ciputat Raya No. 5, Kebayoran Lama').
hospital('jakarta selatan', 'RS Khusus Kanker Siloam Semanggi, Jl. Garnisun No. 2 - 3, Setiabudi').
hospital('jakarta selatan', 'RS Khusus Mata Prof.DR.Isak Salim "Aini", Jl. HR. Rasuna Said, Kuningan').
hospital('jakarta selatan', 'RS Khusus Gigi & Mulut FKG Univ.Prof.DR.Moestopo, Jl. Bintaro Permai Raya No. 3, Bintaro').

% Hospitals in Eastern Jakarta
hospital('jakarta timur', 'RS Khusus Ketergantungan Obat, Jl. Lapangan Tembak No. 75, Cibubur').
hospital('jakarta timur', 'RS Pusat Angkatan Udara Dr. Esnawan Antariksa, Jl. Merpati No. 2, Halim Perdanakusuma').
hospital('jakarta timur', 'RSUD Budhi Asih, Jl. Dewi Sartika III No. 200, Kramat Jati').
hospital('jakarta timur', 'RS TK.III DIK PUSDIKKES KODIKLATAD, Jl. Raya Bogor, Kramat Jati').
hospital('jakarta timur', 'RS Bhayangkara Tk.I  R. Said Sukanto, Jl. RS Polri, Kramat Jati').
hospital('jakarta timur', 'RSUD Pasar Rebo, Jl. Letjen T. B. Simatupang No. 30, Pasar Rebo').
hospital('jakarta timur', 'RS Omni Internasional Pulomas, Jl. Pulomas Barat VI No. 20, Pulogadung').
hospital('jakarta timur', 'RS Khusus Jantung Bina Waluya, Jl. TB Simatupang No. 71 Jak-Tim, Pasar Rebo').
hospital('jakarta timur', 'RSJ Jiwa Islam Klender Jakarta, Jl. Bunga Rampai X - Perumnas Klender, Duren Sawit').
hospital('jakarta timur', 'RS Khusus Orthopaedi & Traumatologi Jakarta, Jl. Haji Ten, Pulogadung').
hospital('jakarta timur', 'RS Khusus Jantung Matraman, Jl. Matraman Raya No.23, Matraman').
hospital('jakarta timur', 'RSIA Sayyidah, Jl. Taman Malaka Selatan No. 6, Duren Sawit').
hospital('jakarta timur', 'RS Khusus Bedah Rawamangun, Jl. Balai Pustaka Raya No. 29-31, Pulogadung').
hospital('jakarta timur', 'RS Persahabatan, Jl. Persahabatan Raya, Pulogadung').
hospital('jakarta timur', 'RSIA Hermina Jatinegara, Jl. Jatinegara Barat No. 126, Jatinegara').
hospital('jakarta timur', 'RSIA Resti Mulya, Jl. Pahlawan Komarudin Raya No. 5, Cakung').
hospital('jakarta timur', 'RS Universitas Kristen Indonesia, Jl. Mayjen Sutoyo No. 2, Cawang').

hospital('kepulauan seribu', 'RSUD Kepulauan Seribu, Pulau Pramuka, Kepulauan Seribu').