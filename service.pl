:- [database].

get_treatments_list(Disease, L) :-
    findall(Treatment, treatment(Disease, Treatment), L).

get_treatments(Disease) :-
    get_treatments_list(Disease, L),
    print_elements(L).

get_symptoms_list(Disease, L) :-
    findall(Symptom, symptom(Disease, Symptom), L).

get_symptoms(Disease) :-
    get_symptoms_list(Disease, L),
    print_elements(L).


get_diseases(_, []) :- !.

get_diseases(Disease, [Head|Tail]) :-
    get_diseases(Disease, Tail), 
    symptom(Disease,Head).

get_all_symptoms :-
    findall(Symptom, symptom(_,Symptom), L),
    sort(L, Sorted),
    print_elements(Sorted).

get_all_diseases :-
    findall(Disease, symptom(Disease,_), L),
    sort(L, Sorted),
    print_elements(Sorted).

get_possible_diseases([]) :- !.

get_possible_diseases([Head|Tail], L) :-
    findall(Disease, get_diseases(Disease, [Head|Tail]), L).

get_possible_treatments([]) :- !.

get_possible_treatments([Head|Tail]) :-
     findall(Treatment, treatment(Head, Treatment), L),
     atom_concat(Head, ' treatments:', Z),
     write(Z), nl,
     print_elements(L), nl,
     get_possible_treatments(Tail).



print_elements([]):-!.

print_elements([Head|Tail]):-
    write(Head),
    nl,
    print_elements(Tail).

get_hospitals_list(Hospital, L) :-
    findall(Hospital, hospital(_, Hospital), L).

get_regions_list(Region, L) :-
     findall(Region, hospital(Region, _),L).

get_all_hospitals:-
    get_hospitals_list(_, L),
    sort(L, Sorted),
    print_elements(Sorted).

get_all_region:-
    get_regions_list(_, L),
    sort(L, Sorted),
    print_elements(Sorted).

get_hospitals_by_region(Region):-
    findall(Hospital, hospital(Region, Hospital),L),
    sort(L, Sorted),
    print_elements(Sorted).

% get_special_hospitals(Disease):-
%     get_hospitals_list(_, L),
%     split_string(Disease," ","", DiseaseWord),
%     (member("heart", DiseaseWord)
%     -> check_hospital_speciality("Jantung", L)
%     ;
%     false
%     ).

% check_hospital_speciality(SpecialDisease, [H|T]):-
%     split_string(H," ","", L),
%     (member(SpecialDisease, L)
%     -> write(H), nl, check_hospital_speciality(SpecialDisease,T)
%     ;
%     check_hospital_speciality(SpecialDisease, T)).